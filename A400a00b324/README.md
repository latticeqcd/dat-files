# A400a00b324

- QCD
- C* boundary conditions in spatial directions, periodic boundary conditions in time
- 3 + 1 flavors: u = d = s, c
- for details, see [First results on QCD+QED with C* boundary conditions](https://doi.org/10.1007/JHEP03(2023)012).

# Data files

- 
